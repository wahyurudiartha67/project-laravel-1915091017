<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$nama}} | {{$page}}</title>

    @yield('css')
    
</head>
<body>
<div class="container-fluid" id="two">
<div class="navbar">
    <h2> {{$nama}} </h2>
<ul>
<li><a href="/">BERANDA</a></li>
<li id="li-tentang"><a href="/about">TENTANG</a></li>
<li id="li-kemampuan"><a href="/skills">KEMAMPUAN</a></li>
<li id="li-pendidikan"><a href="/education">PENDIDIKAN</a></li>
<li id="li-kontak"><a href="/contact">KONTAK</a></li>
</ul>

</div>

@yield('konten')

</div>

</body>
</html>

