@extends('layout2')

@section('css')
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/contact.css">
    <style>
        #li-kontak{
            color: #c32865;
        }
    </style>
@endsection

@section('konten')
   
<div class="content">
    <h1>Kontak <span style="color: #c32865">Saya</span> </h1>
        <div class="container">
        <br>         
            <div class="text-center">
                <p>Silahkan kirimkan pesan teman-teman ke saya.</p>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m25!1m12!1m3!1d2792.889962886002!2d115.0967679420296!3d-8.127640789061799!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m10!3e0!4m3!3m2!1d-8.1265631!2d115.09670729999999!4m4!2s-8.126714768254397%2C%20115.0967125707757!3m2!1d-8.1267148!2d115.09671259999999!5e0!3m2!1sid!2sid!4v1632943605503!5m2!1sid!2sid" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                   
                </div>
                <div class="col-xs-12 col-sm-6">
                    <form class="contact_form_box">
                        <div class="name_email">
                            <input style="background: none; color:white" type="text" name="user_name" class="form-control" id="name" placeholder="Nama Anda" required size="35">
                            <input style="background: none; color:white" type="email" name="user_email" class="form-control" id="email"  placeholder="E-Mail Anda" required size="35">
                        </div><br>
                        <label style="color:white;"for="message_box" >Pesan Anda</label>
                        <textarea style="background: none; color:white" id="message_box" name="user_message" class="form-control" required rows="10" style="resize: vertical;"></textarea>
                        <input value="KIRIM PESAN" class="btn btn-white">
                    </form>                   
                </div>
            </div>
        </div>
    </div>
   

@endsection
       