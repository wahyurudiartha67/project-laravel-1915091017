@extends('layout2')
@section('css')
    <link rel="stylesheet" href="/css/about.css">
    <script src="https://use.fontawesome.com/afd1f3c82a.js"></script>
    <style>
        #li-tentang{
            color: #c32865;
        }
    </style>
@endsection

@section('konten')
    
<div class="content">

    <img src="/gambar/about.png">
    <h1>Tentang <span style="color: #c32865">Saya</span> </h1>
    <p>Nama saya I Gede Wahyu Rudiartha, lahir di Singaraja, Bali pada tanggal 15 September 2001 dan sekarang saya berusia 20 Tahun <br>
    Saya adalah seorang mahasiswa semester 5 program studi Sistem Informasi di Universitas Pendidikan Ganesha <br>
    Saya suka bermain game PC dan menonton film</p>
    <br><br>
    <div class="social">
    <ul>
        <li><a href="https://www.facebook.com" target="_blank"><i class="fa fa-facebook"></i></a></li>
        <li><a href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a></li>                        
        <li><a href="https://www.instagram.com/wahyurudiartha/" target="_blank"><i class="fa fa-instagram"></i></a></li>
        <li><a href="https://www.youtube.com/channel/UC5R414oLpGqgxbRpAXTp-QQ" target="_blank"><i class="fa fa-youtube-play" ></i></a></li>
        </ul>
    </div>
    </div>
    

@endsection
