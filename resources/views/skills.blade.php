@extends('layout2')

@section('css')
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<link rel="stylesheet" href="/css/skills.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-circle-progress/1.2.2/circle-progress.min.js"></script>
<style>
  #li-kemampuan{
      color: #c32865;
  }
</style>
@endsection
  
@section('konten')
    
  <h1>Kemampuan <span style="color: #c32865">Saya</span> </h1>
  
      <div class="wrapper">
        <div class="card">
          <div class="circle">
            <div class="bar"></div>
            <div class="box"><span></span></div>
          </div>
          <div class="text">HTML & CSS</div>
        </div>
        <div class=" card js">
          <div class="circle">
            <div class="bar"></div>
            <div class="box"><span></span></div>
          </div>
          <div class="text">JavaScript</div>
        </div>
        <div class="card react">
          <div class="circle">
            <div class="bar"></div>
            <div class="box"><span></span></div>
          </div>
          <div class="text">React JS</div>
        </div>
        <div class="card editing">
          <div class="circle">
            <div class="bar"></div>
            <div class="box"><span></span></div>
          </div>
          <div class="text">Editing</div>
        </div>
        <div class="card communication">
          <div class="circle">
            <div class="bar"></div>
            <div class="box"><span></span></div>
          </div>
          <div class="text">Communication</div>
        </div>
        <div class="card english">
          <div class="circle">
            <div class="bar"></div>
            <div class="box"><span></span></div>
          </div>
          <div class="text">English</div>
        </div>
      </div>

      <script>
        let options = {
          startAngle: -1.55,
          size: 150,
          value: 0.85,
          fill: {gradient: ['#a445b2', '#fa4299']}
        }
        $(".circle .bar").circleProgress(options).on('circle-animation-progress',
        function(event, progress, stepValue){
          $(this).parent().find("span").text(String(stepValue.toFixed(2).substr(2)) + "%");
        });
        $(".js .bar").circleProgress({
          value: 0.70
        });
        $(".react .bar").circleProgress({
          value: 0.60
        });
        $(".editing .bar").circleProgress({
          value: 0.80
        });
        $(".communication .bar").circleProgress({
          value: 0.85
        });
        $(".english .bar").circleProgress({
          value: 0.82
        });
      </script>


@endsection
   