@extends('layout')

@section('css')
<link rel="stylesheet" href="/css/app.css">
@endsection

@section('konten')
    
<div class="content">
<h4>Personal Profile</h4>
<h1 style="font-size: 80px">Hello, I'm<br> {{$nama}}</h1>
<h4 style="padding-top:20px"><hr style="display: inline-block;width: 50px;border: 2px solid #c32865;"> Web Developer</h4>
<a href="/about"><button  class="btn">Tentang Saya</button></a>
</div>

@endsection