@extends('layout2')

@section('css')
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/education.css">
    <script src="https://use.fontawesome.com/afd1f3c82a.js"></script>
    <style>
        #li-pendidikan{
            color: #c32865;
        }
        #satu{
            padding-left: 300px;
            padding-top: 20px;
        }
        #dua{
            padding-left: 300px;
        }
        #tiga{
            padding-right: 300px;
            padding-top: 20px;
        }
        #empat{
            padding-right: 300px;
        }
    </style>
@endsection
   
@section('konten')
    
<div class="content">
      
    <div class="row">
            <h1>Pengalaman & <span style="color: #c32865">Pendidikan</span></h1>
        <div class="col-lg-6 m-15px-tb">
            <div class="resume-box">
                <ul>
                    <li id="satu">
                        <div class="icon">
                            <i class="fa fa-graduation-cap"></i>
                        </div>
                        <span class="time open-sans-font text-uppercase">2008 - 2014</span>
                        <h5 > <span class="poppins-font text-uppercase" style="color: #c32865" >SD</span> <span class="place open-sans-font">| SD 1 PAKET AGUNG</span></h5>
                        <p class="open-sans-font">Menjalani pendidikan tingkat Sekolah Dasar <br>di SD Negeri 1 Paket Agung selamat 6 tahun</p>
                    </li><br>
                    <li id="dua">
                        <div class="icon">
                            <i class="fa fa-graduation-cap"></i>
                        </div>
                        <span class="time open-sans-font text-uppercase">2014 - 2017</span>
                        <h5 ><span class="poppins-font text-uppercase" style="color: #c32865" >SMP</span> <span class="place open-sans-font">| SMP Negeri 6 Singaraja</span></h5>
                        <p class="open-sans-font">Melanjutkan pendidikan pada tingkat Sekolah Menengah Pertama <br>di SMP Negeri 6 Singaraja</p>
                    </li>
                    
                </ul>
            </div>
        </div>
        <div class="col-lg-6 m-15px-tb">
            <div class="resume-box">
                <ul>
                    <li id="tiga">
                        <div class="icon">
                            <i class="fa fa-graduation-cap"></i>
                        </div>
                        <span class="time open-sans-font text-uppercase">2017 - 2019</span>
                        <h5><span class="poppins-font text-uppercase" style="color: #c32865" >SMA </span><span class="place open-sans-font">| SMA Negeri 4 Singaraja</span></h5>
                        <p class="open-sans-font">Melanjutkan pendidikan pada tingkat SMA di SMA Negeri 4 Singaraja <br>Mengambil jurusan MIPA </p>
                    </li><br>
                    <li id="empat">
                        <div class="icon">
                            <i class="fa fa-graduation-cap"></i>
                        </div>
                        <span class="time open-sans-font text-uppercase">2019 - Sekarang</span>
                        <h5><span class="poppins-font text-uppercase" style="color: #c32865" >Mahasiswa </span><span class="place open-sans-font">| Universitas Pendidikan Singaraja</span></h5>
                        <p class="open-sans-font">Melanjutkan pendidikan pada tingkat perguruan tinggi di Universitas Pendidikan Ganesha <br>Mengambil program studi S1 Sistem Infromasi, Jurusan Teknik Informatika, Fakultas Teknik dan Kejuruan</p>
                    </li>

                </ul>
            </div>
        </div>
    </div>
    
</div>


@endsection
      